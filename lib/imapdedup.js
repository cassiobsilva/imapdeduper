'use strict';
var spawn = require('child_process').spawn;
var path = require('path');
var config = require('config');
var sem = require('semaphore')(3);

class ImapDedup {
  constructor(email, mailbox) {
    this.parameters = [
      `--user=${email.email}`, `--password=${email.password}`,
      `--server=${email.host}`, `--port=${email.port}`,
      config.get('all-headers') ? '-cm' : ''
    ];
    this.mailbox = mailbox ? mailbox : 'ALL';
    this.scriptPath = path.join(__dirname, '../bin/imapdedup.py');
    this.dry = config.get('dry');
    this.errors = [];
  }

  _listMailboxes(callback) {
    var mboxList = [];
    var parameters = this.parameters.concat([`-l`]);

    var mboxListProc = spawn(this.scriptPath, parameters);

    mboxListProc.stdout.setEncoding('utf8');
    mboxListProc.stderr.setEncoding('utf8');

    mboxListProc.stdout.on('data', data => {
      mboxList = data.trim().split('\n');
    });

    mboxListProc.stderr.on('data', data => {
      if (data.includes('Unencrypted connection')) {
        return;
      }

      console.log(data);
    });

    mboxListProc.on('close', () => {
      if (!mboxList.length) {
        return callback('empty mbox list');
      }

      return callback(null, mboxList);
    });
  }

  _getMessagesDeleted(text) {
    var index = this.dry ? 0 : 3;

    return parseInt(text.split(' ')[index], 10);
  }

  _cleanMbox(mbox, callback) {
    var validator = this.dry ? 'would now be marked as \'deleted\'' : 'messages marked as deleted';
    var parameters = this.parameters.concat([mbox]);
    var duplicatedMessages = 0;

    if (this.dry) {
      parameters.push('--dry-run');
    }

    var mboxDedup = spawn(this.scriptPath, parameters);

    mboxDedup.stdout.setEncoding('utf8');
    mboxDedup.stderr.setEncoding('utf8');

    mboxDedup.stdout.on('data', data => {
      var splittedData = data.split('\n');

      splittedData.forEach(line => {
        if (line.includes(validator)) {
          duplicatedMessages = this._getMessagesDeleted(line);
        }
      });
    });

    mboxDedup.stderr.on('data', data => {
      if (data.includes('Unencrypted connection')) {
        return;
      }

      this.errors.push(data);
    });

    mboxDedup.on('close', () => {
      return callback(duplicatedMessages);
    });
  }

  clean(callback) {
    var duplicated = 0;
    var boxes = 0;
    var cleanBoxes = 0;

    this._listMailboxes((err, mboxList) => {
      if (err) {
        return callback(err);
      }

      boxes = mboxList.length;

      mboxList.forEach(mbox => {
        sem.take(() => {
          this._cleanMbox(mbox, duplicatedCount => {
            duplicated += duplicatedCount;
            cleanBoxes++;
            sem.leave();
          });
        });
      });

      var intervalId = setInterval(() => {
        if (boxes !== cleanBoxes) {
          return;
        }

        clearInterval(intervalId);
        return callback(null, duplicated, this.errors);
      }, 500);
    });
  }
}

module.exports = ImapDedup;
