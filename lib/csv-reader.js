'use strict';
var fs = require('fs');
var readline = require('readline');
var config = require('config');

class CsvReader {
  constructor() {
    this.data = [];
    this.file = config.get('file');
    this.delimiter = config.get('delimiter');
  }

  getEmailAccounts(callback) {
    var rl = readline.createInterface({
      input: fs.createReadStream(this.file, {
        encoding: 'utf8'
      })
    });

    rl.on('line', (line) => {
      var splittedLine = line.split(this.delimiter);

      var email = {
        'email': splittedLine[0],
        'host': splittedLine[1],
        'password': splittedLine[2],
        'port': splittedLine[4]
      };

      this.data.push(email);
    });

    rl.on('close', () => {
      return callback(this.data);
    });
  }
}

module.exports = CsvReader;
