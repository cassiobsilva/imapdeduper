'use strict';
var CsvReader = require('./csv-reader');
var ImapDedup = require('./imapdedup.js');

var reader = new CsvReader();
//var errors = [];

reader.getEmailAccounts(data => {
  data.forEach(email => {
    console.log('Cleaning: ', email.email);
    var dedup = new ImapDedup(email);

    dedup.clean((err, duplicated) => {
      if(err) {
        return console.log(err);
      }

      console.log(email.email + ': ' + duplicated + ' messages');
    });
  });
});
